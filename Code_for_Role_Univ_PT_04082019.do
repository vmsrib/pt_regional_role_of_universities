* 1. Import data and define panel
import excel "C:\Users\Vitor Ribeiro\Desktop\PD_final_26032019.xlsx", sheet("Sheet1") firstrow
xtset id t

* 2. Descriptive statistics
summarize GDPpc Perc_Grad_Fem  Grad Electric_energy_cons Total_GVA Labour_productivity ShareHMHFirms

* 3. Correlation matrix
pwcorr GDPpc Perc_Grad_Fem  Grad Electric_energy_cons Total_GVA Labour_productivity ShareHMHFirms, star(1)

* 4. Pooled OLS estimator
reg GDPpc Perc_Grad_Fem  Grad Electric_energy_cons Total_GVA Labour_productivity ShareHMHFirms

* 5. Fixed effects or within estimator
xtreg GDPpc Perc_Grad_Fem  Grad Electric_energy_cons Total_GVA Labour_productivity ShareHMHFirms, fe vce(cluster id)

* 6. Random effects
xtreg GDPpc Perc_Grad_Fem  Grad Electric_energy_cons Total_GVA Labour_productivity ShareHMHFirms, re vce(cluster id) theta

* 7. Hausman test for fixed versus random effects model
quietly xtreg GDPpc Perc_Grad_Fem  Grad Electric_energy_cons Total_GVA Labour_productivity ShareHMHFirms, fe
estimates store fixed
quietly xtreg GDPpc Perc_Grad_Fem  Grad Electric_energy_cons Total_GVA Labour_productivity ShareHMHFirms, re
estimates store random
hausman fixed random, sigmamore
// RULE: p < 0.05 implies rej H0 that RE is preferred to FE
// DECISION: FE is optimal 

* 8. BP test for homocedasticity 
xttest0
// Since the null hypothesis of homocedasticity is rejected at 5%
// then heterocedasticity needs to be controlled for
// by resorting to dynamic PD GMM estimation

* 9. Choose difference or system GMM?
* Follow the rule of thumb of Bond (2001)

* 9.1. POLS estimation with lagged dependent variable
reg GDPpc l.GDPpc Perc_Grad_Fem  Grad Electric_energy_cons Total_GVA Labour_productivity ShareHMHFirms
// Defines the upper bound of l.GDPpc

* 9.2. FE estimation with lagged dependent variable
xtreg GDPpc l.GDPpc Perc_Grad_Fem  Grad Electric_energy_cons Total_GVA Labour_productivity ShareHMHFirms, fe vce(cluster id)
// Defines the lower bound of l.GDPpc

*9.3. Rule: If coefficient of l.GDPpc with difference GMM
*           is close or below the FE estimate
*           then follow system GMM

*9.4. Generate year dummies
tab t, gen(yr)

* 9.5. One-step difference GMM
xtabond2  GDPpc l.GDPpc Perc_Grad_Fem  Grad Electric_energy_cons Total_GVA Labour_productivity ShareHMHFirms yr1-yr5, gmm(l.GDPpc,collapse) iv( Pop_dens SS_Pensions Cul_exp yr7-yr14) noleveleq nodiffsargan robust small

*9.6. Two-step difference GMM
xtabond2  GDPpc l.GDPpc Perc_Grad_Fem  Grad Electric_energy_cons Total_GVA Labour_productivity ShareHMHFirms yr1-yr5, gmm(l.GDPpc,collapse) iv( Pop_dens SS_Pensions Cul_exp yr7-yr14) noleveleq twostep nodiffsargan robust small
// In both cases it follows that the
// coefficient of l.GDPpc under diff GMM is below the coefficient of l.GDPpc under FE
// Hence, system GMM is adopted

* 10. One-step system GMM

* Short-run coefficients
xtabond2 GDPpc l.GDPpc Perc_Grad_Fem  Grad Electric_energy_cons Total_GVA Labour_productivity ShareHMHFirms yr1-yr5, gmm(l.GDPpc, collapse) iv(Pop_dens SS_Pensions yr7-yr14, equation(level)) nodiffsargan robust orthogonal small

* Long-run coefficients
nlcom (_b[Perc_Grad_Fem])/(1-_b[L1.GDPpc])
nlcom (_b[Grad])/(1-_b[L1.GDPpc])
nlcom (_b[Electric_energy_cons])/(1-_b[L1.GDPpc])
nlcom (_b[Total_GVA ])/(1-_b[L1.GDPpc])
nlcom (_b[Labour_productivity])/(1-_b[L1.GDPpc])
nlcom (_b[ShareHMHFirms])/(1-_b[L1.GDPpc])

* 11. Two-step system GMM

* Short-run coefficients
xtabond2 GDPpc l.GDPpc Perc_Grad_Fem  Grad Electric_energy_cons Total_GVA Labour_productivity ShareHMHFirms yr1-yr5, gmm(l.GDPpc, collapse) iv(Pop_dens SS_Pensions yr7-yr14, equation(level)) nodiffsargan twostep robust orthogonal small

* Long-run coefficients
nlcom (_b[Perc_Grad_Fem])/(1-_b[L1.GDPpc])
nlcom (_b[Grad])/(1-_b[L1.GDPpc])
nlcom (_b[Electric_energy_cons])/(1-_b[L1.GDPpc])
nlcom (_b[Total_GVA ])/(1-_b[L1.GDPpc])
nlcom (_b[Labour_productivity])/(1-_b[L1.GDPpc])
nlcom (_b[ShareHMHFirms])/(1-_b[L1.GDPpc])

* 12. Quasi-ML with FE

* Short run coefficients
xtdpdqml GDPpc Perc_Grad_Fem  Grad Electric_energy_cons Total_GVA Labour_productivity ShareHMHFirms, fe  stationary  noconstant vce(robust)

* Long-run coefficients
nlcom (_b[Perc_Grad_Fem])/(1-_b[L1.GDPpc])
nlcom (_b[Grad])/(1-_b[L1.GDPpc])
nlcom (_b[Electric_energy_cons])/(1-_b[L1.GDPpc])
nlcom (_b[Total_GVA])/(1-_b[L1.GDPpc])
nlcom (_b[Labour_productivity])/(1-_b[L1.GDPpc])
nlcom (_b[ShareHMHFirms])/(1-_b[L1.GDPpc])
